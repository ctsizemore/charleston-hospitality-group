<title>Thank you | Charleston Hospitality Group</title>
<link href="css/package-styles.css" rel="stylesheet" type="text/css">

<?php

$my_email = "sales@charlestonhospitalitygroup.com";

/*

Enter the continue link to offer the user after the form is sent.  If you do not change this, your visitor will be given a continue link to your homepage.

If you do change it, remove the "/" symbol below and replace with the name of the page to link to, eg: "mypage.htm" or "http://www.elsewhere.com/page.htm"

*/

$continue = "http://charlestonhospitalitygroup.com";

/*

Step 3:

Save this file (FormToEmail.php) and upload it together with your webpage containing the form to your webspace.  IMPORTANT - The file name is case sensitive!  You must save it exactly as it is named above!  Do not put this script in your cgi-bin directory (folder) it may not work from there.

THAT'S IT, FINISHED!

You do not need to make any changes below this line.

*/

$errors = array();

// Remove $_COOKIE elements from $_REQUEST.

if(count($_COOKIE)){foreach(array_keys($_COOKIE) as $value){unset($_REQUEST[$value]);}}

// Check all fields for an email header.

function recursive_array_check_header($element_value)
{

global $set;

if(!is_array($element_value)){if(preg_match("/(%0A|%0D|\n+|\r+)(content-type:|to:|cc:|bcc:)/i",$element_value)){$set = 1;}}
else
{

foreach($element_value as $value){if($set){break;} recursive_array_check_header($value);}

}

}

recursive_array_check_header($_REQUEST);

if($set){$errors[] = "You cannot send an email header";}

unset($set);

// Validate email field.

if(isset($_REQUEST['email']) && !empty($_REQUEST['email']))
{

if(preg_match("/(%0A|%0D|\n+|\r+|:)/i",$_REQUEST['email'])){$errors[] = "Email address may not contain a new line or a colon";}

$_REQUEST['email'] = trim($_REQUEST['email']);

if(substr_count($_REQUEST['email'],"@") != 1 || stristr($_REQUEST['email']," ")){$errors[] = "Email address is invalid";}else{$exploded_email = explode("@",$_REQUEST['email']);if(empty($exploded_email[0]) || strlen($exploded_email[0]) > 64 || empty($exploded_email[1])){$errors[] = "Email address is invalid";}else{if(substr_count($exploded_email[1],".") == 0){$errors[] = "Email address is invalid";}else{$exploded_domain = explode(".",$exploded_email[1]);if(in_array("",$exploded_domain)){$errors[] = "Email address is invalid";}else{foreach($exploded_domain as $value){if(strlen($value) > 63 || !preg_match('/^[a-z0-9-]+$/i',$value)){$errors[] = "Email address is invalid"; break;}}}}}}

}

// Check referrer is from same site.

if(!(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'],$_SERVER['HTTP_HOST']))){$errors[] = "You must enable referrer logging to use the form";}

// Check for a blank form.

function recursive_array_check_blank($element_value)
{

global $set;

if(!is_array($element_value)){if(!empty($element_value)){$set = 1;}}
else
{

foreach($element_value as $value){if($set){break;} recursive_array_check_blank($value);}

}

}

recursive_array_check_blank($_REQUEST);

if(!$set){$errors[] = "You cannot send a blank form";}

unset($set);

// Display any errors and exit if errors exist.

if(count($errors)){foreach($errors as $value){print "$value<br>";} exit;}

if(!defined("PHP_EOL")){define("PHP_EOL", strtoupper(substr(PHP_OS,0,3) == "WIN") ? "\r\n" : "\n");}

// Build message.

function build_message($request_input){if(!isset($message_output)){$message_output ="";}if(!is_array($request_input)){$message_output = $request_input;}else{foreach($request_input as $key => $value){if(!empty($value)){if(!is_numeric($key)){$message_output .= str_replace("_"," ",ucfirst($key)).": ".build_message($value).PHP_EOL.PHP_EOL;}else{$message_output .= build_message($value).", ";}}}}return rtrim($message_output,", ");}

$message = build_message($_REQUEST);

$message = $message . PHP_EOL.PHP_EOL."-- ".PHP_EOL."";

$message = stripslashes($message);

$subject = "catering inquiry from chg website";

$headers = "From: " . $_REQUEST['email'];

mail($my_email,$subject,$message,$headers);

?>

<!-- include your own success html here -->

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Thank you | Charleston Hospitality Group</title>

    <link rel="shortcut icon" href="favicon.gif">
    <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
    <link href="css/global.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--[endif]-->
    
    <link rel="canonical" href="http://charlestonhospitalitygroup.com">
    <link rel="publisher" href="https://plus.google.com/103070725305044377417">

    
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js">
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24705264-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
    <!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '896758270739348');

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1"

src="https://www.facebook.com/tr?id=896758270739348&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
</head>
<body>
	  <header>
		  <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="logo"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right cl-effect-21">
            <li><a href="index.html" title="Home">Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Company <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="active"><a href="about.html" title="About Us">About Us</a></li>
                <li><a href="locations.html" title="Locations">Locations</a></li>
                <li><a href="catering.html" title="Catering">Catering</a></li>
                <li><a href="sammustafa.html" title="Sam Mustafa">Sam Mustafa</a></li>
                <li><a href="https://dianas.alohaenterprise.com/memberlink/GiftCards.html?companyID=dia06" title="Check E-Card Balance" target="_blank">Check E-Card Balance</a></li>
                <li><a href="salesevents.html" title="Sales and Events">Sales &amp; Events</a></li>
                <li><a href="https://recruiting.paylocity.com/recruiting/jobs/All/7c844750-f56f-4030-b990-3fece9783f51/CHG-LLC" title="Application">Careers</a></li>
                <li><a href="invest.html" title="Investment Opportunities">Investment Opportunities</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">News <span class="caret"></span></a>
              <ul class="dropdown-menu">
	              <li><a href="fullbellyfullhearts.html" title="Full Belly Full Hearts">Full Belly Full Hearts</a></li>
	              <li><a href="https://charlestonhospitalitygroup.wordpress.com" title="News" target="_blank">News</a></li>
	            <li><a href="http://charlestondigest.com" title="Digest">Digest</a></li>
              </ul>
            </li>
            <li><a href="https://charlestonhospitalitygroup.securetree.com/" title="Gift Cards">Gift Cards</a></li>
            <li><a href="contact.html" title="Contact Us">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <img class="img-responsive" src="images/about-img.jpg" alt="About Us" />
	  </header>
	  <div class="container">
	  <div class="row welcome">
  <div class="col-xs-12 col-md-6"><h1 class="text-center header">Form Received Sucessfully</h1>
  <div class="header-divider"></div>
  </div>
    <div class="col-xs-12 col-md-6">
    <article>Thank you for taking the time to fill out our catering inquiry form.  We have successfully received your application and will be in touch shortly.  Have a great day!</article>
  </div>
		  </div>
	  </div>
	  
	  <div class="container-fluid container-fluid-padding">
		  		  
	  </div>
  <div class="newsletter">
	<h1 class="text-center"><span>Ready to Learn More?</span> &nbsp; Sign up for our Newsletter</h1>
<!-- Begin MailChimp Signup Form -->
<div id="newsletter">
<form action="//charlestonhospitalitygroup.us6.list-manage.com/subscribe/post?u=9d57da32b13fc53568606bb3a&amp;id=7ec28057bf" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	    <div class="row">
		    <div class="col-xs-6 col-sm-3">
<div class="mc-field-group">
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name">
</div>
		    </div>
		     <div class="col-xs-6 col-sm-3">
<div class="mc-field-group">
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Last Name">
	</div>
		     </div>
		      <div class="col-xs-6 col-sm-3">
<div class="mc-field-group">
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
</div>
		      </div>
		       <div class="col-xs-6 col-sm-3">

    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9d57da32b13fc53568606bb3a_7ec28057bf" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="submit-button"></div>
    <div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    </div>
    </div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='birthday';fnames[4]='MMERGE4';ftypes[4]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
</div>
</div> <!-- /.row -->
<footer>
	<div class="row">
		<div class="col-xs-12 col-md-3"><h4>Concepts</h4>
		<ul>
		<li><a href="http://toastofcharleston.com" title="Toast!" target="_blank">Toast!</a></li>
		<li><a href="http://elistable.com" title="Eli's Table" target="_blank">Eli's Table</a></li>
		<li><a href="http://charlestonhospitalitycatering.com" title="Charleston Hospitality Catering" target="_blank">Catering</a></li>
		</ul>
		</div>
		<div class="col-xs-12 col-md-3" id="rest-margin">
			<ul>
			<li><a href="http://tabbuli.com" title="Tabbuli" target="_blank">Tabbuli</a></li>
			<li><a href="http://queology.com" title="Queology" target="_blank">Queology</a></li>
			<li><a href="http://honkytonksaloon.com" title="Honky Tonk Saloon" target="_blank">Honky Tonk Saloon</a></li>
			</ul>
		</div>
		<div class="col-xs-12 col-md-3"><h4>Social</h4>
		<ul>
		<li><a href="https://www.facebook.com/CharlestonHospitalityGroup" title="Charleston Hospitality Group Facebook" target="_blank">Facebook</a></li>
		<li><a href="https://www.instagram.com/charlestonhospitalitygroup" title="Charleston Hospitality Group Instagram" target="_blank">Instagram</a></li>
		<li><a href="https://twitter.com/chgcharleston" title="Charleston Hospitality Group Twitter" target="_blank">Twitter</a></li>
		</ul>
		</div>
		<div class="col-xs-12 col-md-3"><h4>Contact</h4>
		<ul>
			<li><a href="tel:8438220011">843.822.0011</a></li>
			<li><a href="https://goo.gl/maps/MpGQasM8VcF2" title="Get Directions" target="_blank">1114 Morrison Drive<br />
			Charleston, SC 29403</a></li>
<li><a href="mailto:chg@charlestonhospitalitygroup.com?Subject=Website%20Inquiry" target="_top">Email us</a></li>
<li><a href="https://dianas.alohaenterprise.com/memberlink/GiftCards.html?companyID=dia06" title="Check E-Card Balance" target="_blank">Check E-Card Balance</a></li>
		</ul>
</div>
	</div>
</footer>
<div class="basement">
			&copy; 2018 <span>Charleston Hospitality Group.</span> All Rights Reserved.
	</div>


	  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
   
  </body>
</html>
